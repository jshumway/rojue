(ns rojue.util)

(defn mmap [f m]
  (into {} (for [[k v] m] (f k v))))