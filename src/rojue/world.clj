(ns rojue.world
  (:use [rojue.util :only [mmap]]))

;; Location Functions.

(defn- all-locs [[width height]]
  ;; Returns a list of all locations in a |width| by |height| grid.
  (for [col (range height) row (range width)]
    (vector row col)))

(defn- translate [loc delta]
  (map + loc delta))

(defn- neighbor-locs [loc]
  (remove (partial = loc)
          (map (partial translate loc)
               (for [x [-1 0 1] y [-1 0 1]] (vector x y)))))

;; World Functions.

(defn- count-surrounding-floors [world loc]
  (count
    (filter (partial = :floor)
            (map #(get world % :wall) (neighbor-locs loc)))))

(defn- smooth-tile [world loc]
  (if (>= (count-surrounding-floors world loc) 4) :floor :wall))

(defn- smooth [world]
  (mmap (fn [loc _] (vector loc (smooth-tile world loc)))
        world))

(defn- generate-random
  "Create a random world with |dims|. World will be composed of floors
  and walls with ~|p|% floors."
  [[width height :as dims] p]
  (zipmap (all-locs dims)
          (repeatedly (fn get-random-tile []
                        (if (< (rand) p) :floor :wall)))))

(defn generate-cave [[width height :as dims] smoothness]
  (loop [world (generate-random dims 0.45) rounds smoothness]
    (if (> rounds 0) (recur (smooth world) (dec rounds))
      world)))

;; Helper Functions

(defn- stringify-row [tiles [_ tile-row]]
  (let [repr (comp :glyph (partial get tiles) last)]
    (apply str (map repr (sort-by first tile-row)))))

(defn group-rows [tiles world]
  (let [get-row (comp last first)]
    (group-by (partial get-row) world)))

(defn stringify [tiles world]
  (map (partial stringify-row tiles)
       (group-rows tiles world)))
