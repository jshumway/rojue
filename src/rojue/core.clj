(ns rojue.core
  (:require [lanterna.screen :as s]
            [rojue.world :as world])
  (:use [rojue.util :only [mmap]]))

;; Some Defs

(def world-width 80)
(def world-height 25)
(def world-dims [world-width world-height])

;;; Tile Defs.

(def tiles
  {:floor {:name "Stone Floor"
           :passable true
           :glyph \.}
   :wall {:name "Stone Wall"
          :passable false
          :glyph \#}})

;; Some early component stuff.

(defn create-world-cmpt [loc]
  {:loc loc
   :vel [0 0]
   :glyph \?})

(defn make-player [loc]
  (assoc (create-world-cmpt loc) :glyph \@))

(defn- update-world-cmpt [world id cmpt]
  ;; For now let's just assume creatures can only ever move one space
  ;; at a time, and only up, down, left, or right.
  (let [new-loc (map + (:loc cmpt) (:vel cmpt))
        get-passable (comp :passable (partial get tiles) (partial get world))]
    (if-not (get-passable new-loc) cmpt
      (assoc cmpt :loc new-loc))))

;; Systems

(defn run-world-system [world components]
  (mmap update-world-cmpt components))

(defn draw-world [screen world]
  (dorun (map-indexed (fn [i row] (s/put-string screen 0 i row))
                      (world/stringify tiles world))))

(defn run-render-system [screen world world-cmpts]
  (draw-world screen world)
  (let [x (comp first :loc)
        y (comp last  :loc)]
    (doseq [[id cmpt] world-cmpts]
      (s/put-string screen (x cmpt) (y cmpt) (str (:glyph cmpt))))))

(defn -main [& args]
  (let [screen (s/get-screen :swing)
        world (world/generate-cave world-dims 3)]
    (s/start screen)
    (run-render-system screen world {1 (make-player [1 1])})
    (s/redraw screen)
    (s/get-key-blocking screen)
    (s/stop screen)))
