(defproject rojue "0.0.1"
  :description "A rogue like in clojure."
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [clojure-lanterna "0.9.4"]]
  :main rojue.core)
